package plugins.fab.timestamp;

import icy.gui.frame.progress.ToolTipFrame;
import icy.plugin.abstract_.PluginActionable;
import icy.sequence.Sequence;

public class Timestamp extends PluginActionable
{

    // IcyFrame mainFrame ;

    @Override
    public void run()
    {
        new ToolTipFrame("<html>" + "<b>TimeStamp added.</b>" + "<br>"
                + "<br>Place your cursor over the TimeStamp's text and type one of the following letters to change the display:"
                + "<br>" + "<li> t : switch from frame to time (using time scale) display"
                + "<li> +/- : increase / decrease font size" + "<li> m : move time stamp to cursor location"
                + "<li> k : remove time stamp" + "</html>");

        final Sequence sequence = getActiveSequence();

        if (sequence != null)
            sequence.addOverlay(new TimeStampOverlay(sequence));
    }
}
