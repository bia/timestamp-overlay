package plugins.fab.timestamp;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.concurrent.TimeUnit;

import icy.canvas.IcyCanvas;
import icy.gui.frame.progress.AnnounceFrame;
import icy.math.MathUtil;
import icy.math.UnitUtil;
import icy.painter.Overlay;
import icy.sequence.Sequence;
import icy.util.GraphicsUtil;
import icy.util.StringUtil;

public class TimeStampOverlay extends Overlay
{
    enum DisplayStyle
    {
        FRAME, TIME
    };

    String timeStampText;
    int x;
    int y;
    Font font;
    DisplayStyle displayStyle = DisplayStyle.FRAME;
    Rectangle2D textBound = new Rectangle2D.Double(0, 0, 0, 0);
    int fontSize = 20;
    int displaySelectionIndex = 0;

    public TimeStampOverlay(Sequence sequence)
    {
        super("Timestamp");

        x = 20;
        y = sequence.getHeight() - 20;
        font = new Font("Arial", Font.BOLD, fontSize);
    }

    /**
     * Display the time with all the units.
     * 
     * @param valueInMs
     *        : value in milliseconds
     * @param displayMs : Even if a unit is not relevant (equals to zero), it will be displayed.
     * @return <b>Example:</b> "2h 3min 40sec 350ms".
     */
    public static String getTimeAsString(double valueInMs, boolean displayMs)
    {
        double v = Math.abs(valueInMs);

        final int day = (int) (v / (24d * 60d * 60d * 1000d));
        v %= 24d * 60d * 60d * 1000d;
        final int hour = (int) (v / (60d * 60d * 1000d));
        v %= 60d * 60d * 1000d;
        final int minute = (int) (v / (60d * 1000d));
        v %= 60d * 1000d;
        final int second = (int) (v / 1000d);
        v %= 1000d;
        final double ms = MathUtil.round(v, 1);

        String result = "";

        if (day > 0)
            result += day + "d ";
        if (hour > 0)
            result += String.format("%2dh ", Integer.valueOf(hour));
        else if (!StringUtil.isEmpty(result))
            result += "00h ";
        if (minute > 0)
            result += String.format("%2dmin ", Integer.valueOf(minute));
        else if (!StringUtil.isEmpty(result))
            result += "00min ";
        if (second > 0)
            result += String.format("%2dsec ", Integer.valueOf(second));
        else if (!StringUtil.isEmpty(result))
            result += "00sec ";
        if (ms > 0d)
            result += String.format("%05.1fms", Double.valueOf(ms));
        else if (displayMs && !StringUtil.isEmpty(result))
            result += String.format("%05.1fms", Double.valueOf(0));

        return result;
    }

    @Override
    public void paint(Graphics2D g, Sequence sequence, IcyCanvas canvas)
    {

        if (g == null)
            return;

        g.setColor(Color.white);
        g.setFont(font);

        switch (displayStyle)
        {
            case FRAME:
                timeStampText = "Current frame: " + canvas.getPositionT();
                break;

            case TIME:
                final double timeIntervalMs = sequence.getTimeInterval() * 1000d;
                final TimeUnit unit = UnitUtil.getBestTimeUnit(timeIntervalMs * 10);
                double ms = canvas.getPositionT() * timeIntervalMs;

                // minute or "higher" time unit ?
                if (unit.ordinal() >= TimeUnit.MINUTES.ordinal())
                    timeStampText = getTimeAsString(ms, (timeIntervalMs % 1000d) != 0d);
                else
                    timeStampText = UnitUtil.displayTimeAsStringWithComma(ms, 3, unit);
                break;
        }

        g.drawString(timeStampText, x, y);

        Rectangle2D r = GraphicsUtil.getStringBounds(g, font, timeStampText);
        r.setRect(r.getX() + x, r.getY() + y, r.getWidth(), r.getHeight());
        textBound = r;
    }

    @Override
    public void keyPressed(KeyEvent e, Point2D imagePoint, IcyCanvas canvas)
    {
        if (e.isConsumed())
            return;

        if (e.getKeyChar() == 'm')
        {
            x = (int) imagePoint.getX();
            y = (int) imagePoint.getY();
            canvas.getSequence().painterChanged(this);
            return;
        }

        // if ( !isPointInTextArea( imagePoint ) ) return;

        if (e.getKeyChar() == 't')
        {
            displaySelectionIndex++;
            if (displaySelectionIndex >= DisplayStyle.values().length)
            {
                displaySelectionIndex = 0;
            }

            displayStyle = DisplayStyle.values()[displaySelectionIndex];

        }

        if (e.getKeyChar() == '+')
        {
            fontSize++;
        }

        if (e.getKeyChar() == '-')
        {
            fontSize--;
            if (fontSize < 2)
                fontSize = 2;
        }

        if (e.getKeyChar() == 'k')
        {
            canvas.getSequence().removePainter(this);
            new AnnounceFrame("Time Stamp removed", 2);
        }

        font = new Font("Arial", Font.BOLD, fontSize);
        canvas.getSequence().painterChanged(this);

        e.consume();
    }
}
